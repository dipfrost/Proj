class Board():
    """Class board with array of board"""

    def __init__(self):
        self.boardArray = [["", "", ""],
                           ["", "", ""],
                           ["", "", ""]]

    def showboard(self):
        for x in self.boardArray:
            print(x)

    def setfigure(self, figure, posx, posy):
        self.boardArray[posx][posy] = figure

    def movevalidation(self, posx, posy):
        if self.boardArray[posx][posy] == "":
            return True
        else:
            return False

    def checkwin(self):
        if self.boardArray[0][0] == self.boardArray[0][1] == self.boardArray[0][2]:
            return self.boardArray[0][0]
        elif self.boardArray[1][0] == self.boardArray[1][1] == self.boardArray[1][2]:
            return self.boardArray[1][0]
        elif self.boardArray[2][0] == self.boardArray[2][1] == self.boardArray[2][2]:
            return self.boardArray[2][0]
        elif self.boardArray[0][0] == self.boardArray[1][0] == self.boardArray[2][0]:
            return self.boardArray[0][0]
        elif self.boardArray[0][1] == self.boardArray[1][1] == self.boardArray[2][1]:
            return self.boardArray[0][1]
        elif self.boardArray[0][2] == self.boardArray[1][2] == self.boardArray[2][2]:
            return self.boardArray[0][2]
        elif self.boardArray[0][0] == self.boardArray[1][1] == self.boardArray[2][2]:
            return self.boardArray[0][0]
        elif self.boardArray[0][2] == self.boardArray[1][1] == self.boardArray[2][0]:
            return self.boardArray[0][2]

    def drowcheck(self):
        res = ""
        for i in range(len(self.boardArray)):
            for j in range(len(self.boardArray)):
                res += self.boardArray[i][j]
        if len(res) == 9:
            return True
        else:
            return False
