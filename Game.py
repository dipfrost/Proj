from Player import Player
from Board import Board

board = Board()
move = 1

print("Write name of first player:")
firstPlayer = Player(input(), "X")
print("Write name of second player:")
secondPlayer = Player(input(), "O")

print("--- GAME START ---")

# ------------ MAin
board.showboard()
while True:

    # -----INPUT DATA
    hod = input()
    # ------------------MOVE

    if move % 2 == 0:
        if board.movevalidation(int(hod.split()[0]), int(hod.split()[1])):
            board.setfigure(secondPlayer.getfigure(), int(hod.split()[0]), int(hod.split()[1]))
            move += 1
        else:
            print("Error move")
    else:
        if board.movevalidation(int(hod.split()[0]), int(hod.split()[1])):
            board.setfigure(firstPlayer.getfigure(), int(hod.split()[0]), int(hod.split()[1]))
            move += 1
        else:
            print("Error move")
# ------CHECK WIN
    if board.checkwin() == "X":
        print("Player: " + firstPlayer.getname() + " WIN!!!!!")
        board.showboard()
        break
    elif board.checkwin() == "O":
        print("Player: " + secondPlayer.getname() + " WIN!!!!!")
        board.showboard()
        break
# ------CHECK DRAW
    if board.drowcheck():
        print("Drow")
        break
    board.showboard()
    ## ------------------END MOVE BLOCK
print("--- GAME END ---")
